const express = require("express");
const cors = require("cors");
const app = express();

require('./src/index.js')(app)


const PORT = 8000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
