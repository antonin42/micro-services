const express = require("express");
const helmet = require("helmet");
const log4js = require("log4js");
const cors = require("cors");
const appLogger = log4js.getLogger();
const path = require("path");

module.exports = function (app) {
  app.use(cors());
  app.use(helmet());
  app.use(express.json());
  app.use(log4js.connectLogger(appLogger));

  app.set("views", path.join(__dirname, "Medias"));
  app.set("view engine", "ejs");

  require("./routes/router")(app);
};
