const express = require("express");
const router = require("express").Router();
const path = require("path");

const addFiles = require("./addFile");
const getFiles = require("./getFile");
const hello = require("./Debug/hello");

module.exports = function (app) {
  app.use("/", hello);
  app.use("/api/media", addFiles);
  app.use("/api/media", getFiles);
  app.use("/api/media/images", express.static("./test"));
};
