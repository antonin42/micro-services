const express = require("express");
const router = express.Router();
const multer = require("multer");
const path = require("path");
var fs = require("fs");

router.get("/:file_id", (req, res, next) => {
  const fileName = req.params.file_id;
  const directoryPath = path.join("./Medias/");

  // fs.readFile(directoryPath + req.url, function (err, data) {
  //   if (err) {
  //     res.writeHead(404);
  //     res.end(JSON.stringify(err));
  //     return;
  //   }
  //   res.writeHead(200);

  //   res.end(data);
  // });

  // var readable = fs.createReadStream(directoryPath + fileName);
  // res.send(readable);

  var options = {
    root: directoryPath,
  };

  res.sendFile(fileName, options, function (err) {
    if (err) {
      next(err);
    } else {
      console.log("Sent:", fileName);
      next();
    }
  });

  //   res.download(directoryPath + fileName, fileName, (err) => {
  //     if (err) {
  //       res.status(500).send({
  //         message: "Could not download the file. ",
  //       });
  //     }
  //   });
});

module.exports = router;
