# Micro Services

## Description

---

This project is a social network with the following features:

- [x] Account system
- [x] Post System
- [ ] Comment System
- [ ] Notification
- [ ] Chat

## Getting started

---

#### General API

Using **Python ( Flask )**

Available at the following address `http://localhost:5000/api/`

#### File Server

- NodeJS ( ExpressJS )

**Upload file** feature is only available by the General API

**Download file** feature is available at the following address `http://localhost:8000/api/media/`

The whole project uses a docker-compose

## Usage

---

To start the project run
`docker-compose up`
