
from flask import Blueprint

from app.routes.auth.login import login
from app.routes.auth.register import register
from app.routes.auth.token import token

from app.routes.users.me import me
from app.routes.users.edit import edit
from app.routes.users.empty_account import account_empty
from app.routes.users.delete_account import account_delete

from app.routes.posts.create_posts import create_posts
from app.routes.posts.get_posts import get_posts
from app.routes.posts.edit_posts import edit_posts


router = Blueprint('router', __name__)


# AUTHENTICATION ROUTES
router.register_blueprint(register, url_prefix="/auth")
router.register_blueprint(login, url_prefix="/auth")
router.register_blueprint(token, url_prefix="/auth/token")


# USERS ROUTES
router.register_blueprint(me, url_prefix="/users")
router.register_blueprint(edit, url_prefix="/users/edit")
router.register_blueprint(account_empty, url_prefix="/users/empty")
router.register_blueprint(account_delete, url_prefix="/users/delete")


# POSTS ROUTES
router.register_blueprint(create_posts, url_prefix="posts")
router.register_blueprint(get_posts, url_prefix="posts")
router.register_blueprint(edit_posts, url_prefix="posts")
