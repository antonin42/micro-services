from flask import request, jsonify, Blueprint
from mongoengine.errors import ValidationError
from flask_jwt_extended import jwt_required, get_jwt_identity
from app.models.Posts import Posts, Author
from app.models.Users import Users
from app.errors import *

from app.middleware.bouncer import ban_checking


edit_posts = Blueprint('edit_posts', __name__)


@edit_posts.route('/<post_id>', methods=['PUT'])
@jwt_required()
@ban_checking
def create_post_(post_id):
    current_user_id = get_jwt_identity()
    body = request.get_json()
    try:
        if (len(body) != 3):
            return register_field_invalid(), 403

        post = Users.objects(id=post_id).first()

        if not post.author.user_id == current_user_id:
            return access_denied(), 401
        post.title = body['title']
        post.description = body['description']
        post.tags = body['tags']

        post.save()

        return jsonify(post), 201
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403
