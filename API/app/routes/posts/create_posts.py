from flask import request, jsonify, Blueprint
from mongoengine.errors import ValidationError
from flask_jwt_extended import jwt_required, get_jwt_identity
from app.models.Posts import Posts, Author, Media
from app.models.Users import Users
from app.errors import *
import requests

from app.middleware.bouncer import ban_checking

import logging

logger = logging.getLogger('werkzeug')  # grabs underlying WSGI logger


create_posts = Blueprint('create_posts', __name__)


@create_posts.route('/', methods=['POST'])
@jwt_required()
@ban_checking
def create_post_():
    current_user_id = get_jwt_identity()
    try:
        logger.info(request.form['title'])
        logger.info(request.form['description'])
        logger.info(request.form['tags'])
        logger.info(request.files['file'].filename)
        logger.info(request.files['file'].mimetype)

        current_user = Users.objects(id=current_user_id).first()

        internal_url = "http://file_server:8000/api/media"
        external_url = "http://127.0.0.1:8000/api/media/"

        payload = {}
        files = [
            ('file', (request.files['file'].filename,
             request.files['file'], request.files['file'].mimetype))
        ]
        headers = {}

        response = requests.request(
            "POST", internal_url, headers=headers, data=payload, files=files)

        logger.info(response.text)
        if response.status_code == 200:
            post_media = Media(
                mimetype=response.json().get('mimetype'),
                filename=response.json().get('filename'),
                uri=external_url + response.json().get('filename')
            )

            post_author = Author(
                user_id=current_user_id,
                username=current_user.username
            )
            post = Posts(
                title=request.form['title'],
                description=request.form[
                    'description'],
                tags=request.form['tags'].split(";"),
                author=post_author,
                media=post_media
            )
            post = post.save()
        else:
            return jsonify({"msg": "nope"}), 201
        return jsonify({"msg": "Post successfully created"}), 201
    except ValidationError as error:
        return jsonify({'error': error.message}), 403
