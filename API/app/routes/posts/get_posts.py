from flask import request, jsonify, Blueprint
from mongoengine.errors import ValidationError
from flask_jwt_extended import jwt_required, get_jwt_identity
from app.models.Posts import Posts, Author
from app.models.Users import Users
from app.errors import *
from flask_cors import cross_origin
from app.middleware.bouncer import ban_checking


get_posts = Blueprint('get_posts', __name__)


@get_posts.route('/me', methods=['GET'])
@jwt_required()
@ban_checking
def get_my_posts_():
    try:
        current_user_id = get_jwt_identity()

        posts = Posts.objects(author__user_id=current_user_id)

        return jsonify(posts), 201
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403


@get_posts.route('/', methods=['GET'])
@jwt_required()
@ban_checking
@cross_origin()
def get_all_posts_():
    try:

        posts = Posts.objects()

        return jsonify(posts), 201
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403


@get_posts.route('/<post_id>', methods=['GET'])
@jwt_required()
@ban_checking
def get_one_posts_(post_id):
    try:

        post = Posts.objects(id=post_id).first()

        return jsonify(post), 201
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403
