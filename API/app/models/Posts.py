from datetime import datetime
from mongoengine import Document, EmbeddedDocument
from mongoengine import DateTimeField
from mongoengine.fields import EmbeddedDocumentField, StringField, ListField
from config import MONGO_DB_COMMON_ALIAS


class Author(EmbeddedDocument):
    user_id = StringField(required=False)
    username = StringField(required=False)


class Media(EmbeddedDocument):
    mimetype = StringField(required=False)
    filename = StringField(required=False)
    uri = StringField(required=False, unique=False)


class Posts(Document):
    author = EmbeddedDocumentField(Author)
    media = EmbeddedDocumentField(Media)
    title = StringField(required=True, default="New post")
    description = StringField(required=False, default="...")
    tags = ListField(StringField(required=False), default=[])
    created = DateTimeField(default=datetime.now)
    meta = {'db_alias': MONGO_DB_COMMON_ALIAS, 'collection': 'posts'}
