from config import app
from werkzeug.exceptions import HTTPException
from flask import jsonify
import sys
import os
from app.router import router

sys.path.append(os.path.abspath(os.path.join('.', 'app')))

app.register_blueprint(router, url_prefix="/api")


@app.errorhandler(Exception)
def handle_error(error):
    code = 500
    if isinstance(error, HTTPException):
        code = error.code
    return jsonify(error=str(error)), code


app.run(host="0.0.0.0")
