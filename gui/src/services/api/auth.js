import Auth from "../auth";
var axios = require("axios");

export async function login(email, password) {
  var data = {
    email: email,
    password: password,
  };

  var config = {
    method: "post",
    url: "http://localhost:5000/api/auth/login",
    headers: {
      "Content-Type": "application/json",
    },
    data: data,
  };

  axios(config).then(function (response) {
    if (response.status === 200) {
      Auth.authenticate(response.data.access_token);
      window.location = "/";
    } else {
      return false;
    }
  });
}

export async function register(username, email, password) {
  var data = {
    username: username,
    email: email,
    password: password,
  };

  var config = {
    method: "post",
    url: "http://localhost:5000/api/auth/register",
    headers: {
      "Content-Type": "application/json",
    },
    data: data,
  };

  axios(config).then(function (response) {
    if (response.status === 200) {
      Auth.authenticate(response.data.access_token);
      window.location = "/";
    } else {
      return false;
    }
  });
}
