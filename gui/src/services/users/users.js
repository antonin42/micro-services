var axios = require("axios");

export async function get_me() {
  const user_token = localStorage.getItem("user");
  var config = {
    method: "get",
    url: "http://localhost:5000/api/users/me",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + user_token,
    },
  };

  const response = await axios(config);

  if (response.status === 201) {
    console.log(response);
    console.log("test");

    return response;
  } else {
    return false;
  }
}