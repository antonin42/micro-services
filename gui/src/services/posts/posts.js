var axios = require("axios");

export async function get_all_posts() {
  const user_token = localStorage.getItem("user");
  var config = {
    method: "get",
    url: "http://localhost:5000/api/posts/",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
      Authorization: "Bearer " + user_token,
    },
  };

  const response = await axios(config);

  if (response.status === 201) {
    console.log(response.data);
    console.log("test");

    return response.data;
  } else {
    return false;
  }
}

export async function get_all_my_posts() {
  const user_token = localStorage.getItem("user");
  var config = {
    method: "get",
    url: "http://localhost:5000/api/posts/me",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
      Authorization: "Bearer " + user_token,
    },
  };

  const response = await axios(config);

  if (response.status === 201) {
    console.log(response);
    console.log("test2");

    return response.data;
  } else {
    return false;
  }
}