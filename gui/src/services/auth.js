const Auth = {
  authenticate(user) {
    localStorage.setItem("user", user);
  },
  signout() {
    localStorage.removeItem("user");
  },
  getAuth() {
    if (
      localStorage.getItem("user") !== null &&
      localStorage.getItem("user") !== ""
    ) {
      return localStorage.getItem("user");
    }
    return false;
  },
};
export default Auth;
