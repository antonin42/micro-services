/* eslint-disable react-hooks/exhaustive-deps */
import * as React from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import Home from "./views/home";
import Login from "./views/login";
import Profile from "./views/profile";
import About from "./views/about";
import Chat from "./views/chat";

import Navbar from "./componnents/navbar";

import Auth from "./services/auth";
import "./styles/main.css";
import CreatePost from "./views/createPost";

function App() {
  const theme = React.useMemo(() =>
    createTheme({
      palette: {
        mode: "dark",
      },
    })
  );
  return (
    <ThemeProvider theme={theme}>
      <Router>
        {Auth.getAuth && <Navbar />}
        <Routes>
          <Route exact path="/auth" element={<Login />}></Route>
          <Route
            path="/"
            element={
              <Protected>
                <Home />
              </Protected>
            }
          />
          <Route
            path="/profile"
            element={
              <Protected>
                <Profile />
              </Protected>
            }
          ></Route>
          <Route
            path="/chat"
            element={
              <Protected>
                <Chat />
              </Protected>
            }
          ></Route>
          <Route
            path="/post"
            element={
              <Protected>
                <CreatePost />
              </Protected>
            }
          ></Route>
          <Route path="/about" element={<About />}></Route>
        </Routes>
      </Router>
    </ThemeProvider>
  );
}

const Protected = ({ children }) => {
  if (Auth.getAuth === false) {
    return <Navigate to="/auth" replace />;
  }
  return children;
};

export default App;
