import * as React from "react";
import { styled } from "@mui/material/styles";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Collapse from "@mui/material/Collapse";
import Avatar from "@mui/material/Avatar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import { purple } from "@mui/material/colors";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ShareIcon from "@mui/icons-material/Share";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { Grid, Stack } from "@mui/material";

import { get_all_posts } from "../services/posts/posts";
import { Box } from "@mui/system";

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
  marginLeft: "auto",
  transition: theme.transitions.create("transform", {
    duration: theme.transitions.duration.shortest,
  }),
}));

function PostCard(props) {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card sx={{ minWidth: 345 }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: purple[500] }} aria-label="recipe">
            {props.content.author &&
              String(props.content.author["username"])[0].toUpperCase()}
          </Avatar>
        }
        title={props.content.title}
        // subheader={props.content.created}
      />
      <CardMedia
        component="img"
        height="194"
        crossorigin=""
        image={props.content.media && String(props.content.media["uri"])}
        alt="Paella dish"
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {props.content.description}
        </Typography>
        <Box direction="row">
          <Typography variant="caption" color="text.secondary">
            {props.content.tags &&
              props.content.tags.map((tag, index) => <span>{tag} </span>)}
          </Typography>
        </Box>
      </CardContent>
      <CardActions disableSpacing>
        <Typography variant="body1" color="text.secondary">
          Comments
        </Typography>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>Content</Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}

function Home() {
  const [posts, setPosts] = React.useState([{}]);

  React.useEffect(() => {
    async function setup() {
      setPosts(await get_all_posts());
    }
    setup();
  }, []);
  return (
    <React.Fragment>
      <Grid item container justifyContent="center" alignItems="center">
        <Stack spacing={1}>
          {" "}
          {posts.length > 0 &&
            posts.map((post, index) => (
              <p key={index}>
                <div>
                  <p>
                    <PostCard content={post} />
                  </p>
                </div>
              </p>
            ))}
        </Stack>
      </Grid>
    </React.Fragment>
  );
}

export default Home;
