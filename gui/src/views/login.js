import * as React from "react";
import {
  Box,
  Grid,
  Button,
  Stack,
  Tab,
  Tabs,
  Typography,
  TextField,
} from "@mui/material";
import PropTypes from "prop-types";

import { login, register } from "../services/api/auth";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

function LoginForm() {
  const [email, setEmail] = React.useState("antonin@antonin.axx");
  const [password, setPassword] = React.useState("Password!1");
  return (
    <Stack spacing={4}>
      <TextField
        id="filled-read-only-input"
        label="Login"
        variant="standard"
        value="antonin@antonin.axx"
        onChange={(e) => setEmail(e.target.value)}
        required="true"
      />
      <TextField
        id="standard-password-input"
        label="Password"
        type="password"
        autoComplete="current-password"
        variant="standard"
        value="Password!1"
        onChange={(e) => setPassword(e.target.value)}
        required="true"
      />
      <Button
        variant="outlined"
        size="large"
        onClick={async () => {
          await login(email, password);
        }}
      >
        Login
      </Button>
    </Stack>
  );
}

function RegisterForm() {
  const [username, setUsername] = React.useState("antonin");
  const [email, setEmail] = React.useState("antonin@antonin.axx");
  const [password, setPassword] = React.useState("Password!1");

  return (
    <Stack spacing={4}>
      <TextField
        id="filled-read-only-input"
        label="Login"
        variant="standard"
        onChange={(e) => setUsername(e.target.value)}
      />
      <TextField
        id="filled-read-only-input"
        label="Email"
        variant="standard"
        onChange={(e) => setEmail(e.target.value)}
      />
      <TextField
        id="standard-password-input"
        label="Password"
        type="password"
        autoComplete="current-password"
        variant="standard"
        onChange={(e) => setPassword(e.target.value)}
      />
      <Button
        variant="outlined"
        size="large"
        onClick={async () => {
          await register(username, email, password);
        }}
      >
        Register
      </Button>
    </Stack>
  );
}

export default function Login() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <React.Fragment>
      <Grid
        item
        container
        sx={{ height: "95vh" }}
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <Stack spacing={4}>
          <h1>Micro Service Journey</h1>

          <Box sx={{ width: "100%" }}>
            <Box sx={{ borderColor: "divider" }}>
              <TabPanel value={value} index={0}>
                <LoginForm />
              </TabPanel>
              <TabPanel value={value} index={1}>
                <RegisterForm />
              </TabPanel>
              <Tabs value={value} onChange={handleChange} centered="true">
                <Tab label="Login" {...a11yProps(0)} />
                <Tab label="Register" {...a11yProps(1)} />
              </Tabs>
            </Box>
          </Box>
        </Stack>
      </Grid>
    </React.Fragment>
  );
}
