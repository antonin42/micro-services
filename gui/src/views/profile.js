import * as React from "react";
import { styled } from "@mui/material/styles";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Collapse from "@mui/material/Collapse";
import Avatar from "@mui/material/Avatar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { purple } from "@mui/material/colors";

import { Box } from "@mui/system";

import { get_all_my_posts } from "../services/posts/posts";

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
  marginLeft: "auto",
  transition: theme.transitions.create("transform", {
    duration: theme.transitions.duration.shortest,
  }),
}));

function PostCard(props) {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card sx={{ minWidth: 345 }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: purple[500] }} aria-label="recipe">
            {props.content.author &&
              String(props.content.author["username"])[0].toUpperCase()}
          </Avatar>
        }
        title={props.content.title}
        // subheader={props.content.created}
      />
      <CardMedia
        component="img"
        height="194"
        crossorigin=""
        image={props.content.media && String(props.content.media["uri"])}
        alt="Paella dish"
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {props.content.description}
        </Typography>
        <Box direction="row">
          <Typography variant="caption" color="text.secondary">
            {props.content.tags &&
              props.content.tags.map((tag, index) => <span>{tag} </span>)}
          </Typography>
        </Box>
      </CardContent>
      <CardActions disableSpacing>
        <Typography variant="body1" color="text.secondary">
          Comments
        </Typography>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>Content</Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}

function Profile() {
  const [posts, setPosts] = React.useState([{}]);

  React.useEffect(() => {
    async function setup() {
      setPosts(await get_all_my_posts())
    }
    setup();
  }, []);
  return (
    <div>
      <div>
        <Card sx={{ width: 400, margin: "auto", textAlign: "center"}}>
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              Welcome on your profile
            </Typography>
          </CardContent>
        </Card>
      </div>
      <div>
        {posts.length > 0 &&
          posts.map((post, index) => (
            <p key={index}>
              <div>
                <p>
                  <PostCard content={post} />
                </p>
              </div>
            </p>
        ))}
      </div>
    </div>
  );
}

export default Profile;
