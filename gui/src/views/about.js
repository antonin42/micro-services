/* eslint-disable jsx-a11y/heading-has-content */
import React from "react";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

function About() {
  return (
    <Card sx={{ width: 400, margin: "auto", textAlign: "center"}}>
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          About our application
        </Typography>
        <Typography variant="body2" color="text.secondary">
          it's an application that looks like instagram.
        </Typography>
        <Typography variant="body2" color="text.secondary">
          On this application you can publish, comment on posts.
        </Typography>
      </CardContent>
    </Card>
  );
}

export default About;